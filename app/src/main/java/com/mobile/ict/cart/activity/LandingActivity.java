package com.mobile.ict.cart.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.alertdialogpro.AlertDialogPro;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.mobile.ict.cart.LokacartApplication;
import com.mobile.ict.cart.R;
import com.mobile.ict.cart.TourCheck.DemoLists;
import com.mobile.ict.cart.container.MemberDetails;
import com.mobile.ict.cart.database.DBHelper;
import com.mobile.ict.cart.util.Extra;
import com.mobile.ict.cart.util.GetJSON;
import com.mobile.ict.cart.util.Master;
import com.mobile.ict.cart.util.Material;
import com.mobile.ict.cart.util.SharedPreferenceConnector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class LandingActivity extends AppCompatActivity {

    static int from;
    DBHelper dbHelper;
    static String goTo;
    Button bTalkToUs,bRegisteredUser;
    // View authenticationDialogView;
    // AlertDialogPro mobileVerificationAlert;
    OTPCountDownTimer countDownTimer;
    String otp_check;
    long session;
    EditText eMobileNumber,eOtp;
    Button cancel,proceed,verify;
    ImageView imgVerify;
    TextView verifyText;
    Dialog authenticationDialogView;
    private Tracker mTracker;
    private static final String TAG = "LandingActivity";
    String name = "Landing";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Obtain the shared Tracker instance.
        LokacartApplication application = (LokacartApplication) getApplication();
        mTracker = application.getDefaultTracker();
        Log.i(TAG, "Setting screen name: " + name);
        mTracker.setScreenName("Screen~" + name);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        dbHelper = new DBHelper(this);
//        Log.e("Landing page act", "in onCreate()");
        setContentView(R.layout.activity_landing_page);
        Activity ac = getParent();
        bTalkToUs = (Button) findViewById(R.id.bTalkToUs);
        bTalkToUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LandingActivity.this, TalkToUsActivity.class));
                LandingActivity.this.finish();
            }
        });
        bRegisteredUser = (Button) findViewById(R.id.bRegisteredUser);
        bRegisteredUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //startActivity(new Intent(LandingActivity.this, TalkToUsActivity.class));
                //LandingActivity.this.finish();

                openAuthenticationDialog();

            }
        });
    }
    private void storeUserDetails(JSONObject rootJson){



        //store user details here
        String name, address,lastname , pincode , phonenumber , email , abbr , organisation, password , profilePicUrl;
        name = address = lastname = pincode = phonenumber = email = abbr = organisation =profilePicUrl= "null";


        try {
            name = rootJson.getString(Master.ACCEPT_REFERRAL_USER_NAME_TAG);
            address  = rootJson.getString(Master.ACCEPT_REFERRAL_USER_ADDRESS_TAG);
            lastname  = rootJson.getString(Master.ACCEPT_REFERRAL_USER_LAST_NAME_TAG);
            pincode  = rootJson.getString(Master.ACCEPT_REFERRAL_PINCODE_TAG);
            phonenumber  = rootJson.getString(Master.ACCEPT_REFERRAL_USER_NUMBER_TAG);
            password = rootJson.getString("token");
            email  = rootJson.getString(Master.ACCEPT_REFERRAL_USER_EMAIL_TAG);
            if(rootJson.has(Master.ACCEPT_REFERRAL_ORGANISATION_REFERRED_ABBR_TO_TAG)&&rootJson.has(Master.ACCEPT_REFERRAL_ORGANISATION_REFERRED_TO_TAG)) {
                abbr = rootJson.getString(Master.ACCEPT_REFERRAL_ORGANISATION_REFERRED_ABBR_TO_TAG);
                organisation = rootJson.getString(Master.ACCEPT_REFERRAL_ORGANISATION_REFERRED_TO_TAG);
            }

            if(rootJson.has(Master.ACCEPT_REFERRAL_USER_PROFILE_PIC_URL)){
                profilePicUrl  = rootJson.getString(Master.ACCEPT_REFERRAL_USER_PROFILE_PIC_URL);

            }
            MemberDetails.setProfileImageUrl(profilePicUrl);
            MemberDetails.setFname(name);
            MemberDetails.setAddress(address);
            MemberDetails.setLname(lastname);
            MemberDetails.setPincode(pincode);
            MemberDetails.setMobileNumber(phonenumber.substring(2));
            MemberDetails.setEmail(email);
            MemberDetails.setSelectedOrgAbbr(abbr);
            MemberDetails.setSelectedOrgName(organisation);
            MemberDetails.setPassword(password);



            //call db helper to store it to database


            //check if selected org is already present
            String selectedOrg[]  = dbHelper.getSelectedOrg(MemberDetails.getMobileNumber());

            //add the details about the user to database
            dbHelper.addProfile();


            if(selectedOrg==null||selectedOrg[0].equals("null")){

                String organisation2Name = "";
                String organisation2Abbr = "";

                //in the new version check api if we give the number of user the response will
                // contain list of organisations he is part of
                //Jsonarray memberships will only be present if we call the new version check api with user mobile number
                if(rootJson.has("orglist")){
                    JSONArray organisationsList= rootJson.getJSONArray("orglist");
                    JSONObject org = organisationsList.getJSONObject(0);
                    organisation2Name = org.getString("organization");
                    organisation2Abbr = org.getString("abbr");

                }

                if(organisation2Name.equals("")||organisation2Name.equals("null")
                        ||organisation2Abbr.equals("")||organisation2Abbr.equals("null")){
                    dbHelper.setSelectedOrg(phonenumber,organisation,abbr);
                }else{
                    dbHelper.setSelectedOrg(MemberDetails.getMobileNumber(),organisation2Name,organisation2Abbr);
                }


            }else{
                //
                dbHelper.setSelectedOrg(MemberDetails.getMobileNumber(),selectedOrg[1],selectedOrg[0]);



            }

            dbHelper.getTable();

//            Log.e("--------", "");



        } catch (JSONException e) {
            e.printStackTrace();
        }




    }


    private void processResponseFromAcceptReferral(JSONObject response) {

        String acceptReferralResponseStatus = "";
        String detailsPresent = "";

        try {
            detailsPresent = response.getString(Master.ACCEPT_REFERRAL_DETAILS_PRESENT);
            acceptReferralResponseStatus  = response.getString(Master.ACCEPT_REFERRAL_RESPONSE_TAG);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //accept referral successful
        if(acceptReferralResponseStatus.equals("success")){

            try {
                String organisationName = response.getString(Master.ACCEPT_REFERRAL_ORGANISATION_REFERRED_TO_TAG);
                Toast.makeText(getApplicationContext(),"You are now a member of"+organisationName+" this organisation",Toast.LENGTH_SHORT).show();



            } catch (JSONException e) {
                e.printStackTrace();
                //accept referral successful , if you don't see the new organisation in the organisations page please click on the link again
            }



        }else if (acceptReferralResponseStatus.equals("Already a member")){
            //this case would occur if user click on the referral link again
            Toast.makeText(getApplicationContext(),"You are already a member of this organisation",Toast.LENGTH_SHORT).show();


            //check if user details are present
            //redirect to select organisation page

        }else{

        }

        //check if user details are present
        if(detailsPresent.equals("0")){
            //details present
            //redirect to change organisation page
            ShowStepper("organisation");

        }else{
            //details not present
            //show stepper and open profile page to get user data
            ShowStepper("profile");

        }

    }


    public void forceUpdateDialog(){


        //storing the update required in shared pref , if it is set to 2 ,
        // we show a forceupdate dialog even if net is not available
        SharedPreferenceConnector.writeInteger(getApplicationContext(), Master.UpdateRequiredPref , 2);

        AlertDialogPro.Builder builder = new AlertDialogPro.Builder(this);
        //  Chain together various setter methods to set the dialog characteristics
        builder.setMessage(R.string.force_update_alert_dialog_message)
                .setTitle(R.string.force_update_alert_dialog_title);
        builder.setPositiveButton(R.string.force_update_alert_dialog_positive_button, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                //open play store so that the user can update the app
                final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                try {
                    //this will open play store and show lokacart app
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    //if opening in play store fails open play store in browser
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }

            }
        });
        builder.setNegativeButton(R.string.force_update_alert_dialog_negative_button, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //close the app if user clicks close
                finish();
            }
        });

        builder.setCancelable(false);

        //Get the AlertDialogPro from create()
        AlertDialogPro dialog = builder.create();
        dialog.show();
    }

    private void showRecommendUpdateDialog(int calledFrom){
        from= calledFrom;

        //storing the update required in shared pref , if it is set to 1 ,
        // we show a recommend dialog even if net is not available
        SharedPreferenceConnector.writeInteger(getApplicationContext(), Master.UpdateRequiredPref , 1);


        AlertDialogPro.Builder builder = new AlertDialogPro.Builder(this);
        //  Chain together various setter methods to set the dialog characteristics
        builder.setMessage(R.string.force_update_alert_dialog_message)
                .setTitle(R.string.force_update_alert_dialog_title);
        builder.setPositiveButton(R.string.force_update_alert_dialog_positive_button, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                //open play store so that the user can update the app
                final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }

            }
        });
        builder.setNegativeButton(R.string.recommend_update_alert_dialog_negative_button, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //ignore update dialog and continue with the app



                if(from==1){
                    //called from post execute of accept referral


                }else{
                    //called from post execute of version check , Accept referral
                    ShowStepper(goTo);
                }


            }
        });

        builder.setCancelable(false);
        //Get the AlertDialogPro from create()
        AlertDialogPro dialog = builder.create();
        dialog.show();

    }

    private void ShowStepper(@NonNull String nextPage){
        if (SharedPreferenceConnector.readBoolean(getApplicationContext(), Master.STEPPER, Master.DEFAULT_STEPPER)) {

            Intent i = new Intent(getApplicationContext(), StepperActivity.class);
            i.putExtra("redirectto" , nextPage);
            switch(nextPage){
                case "profile":{
                    i.putExtra("showintro" , "true");

                    break;
                }
                case "organisation":{
                    i.putExtra("showintro" , "true");
                    break;
                }
                case "dashboard":{
                    i.putExtra("showintro" , "true");
                    break;
                }
                case "talktous":{

                    break;
                }


            }

            startActivity(i);
            finish();
        } else {
            goToActivity(nextPage);
        }
    }

    private void goToActivity(@NonNull String nextPage) {
        Intent i;
        if (SharedPreferenceConnector.readBoolean(getApplicationContext(), Master.INTRO, Master.DEFAULT_INTRO)){
            switch(nextPage){
                case "profile":{
                    //go to profile activity
                    i = new Intent(LandingActivity.this, DemoLists.class);
                    i.putExtra("redirectto" , "profile");
                    startActivity(i);
                    finish();
                    break;
                }
                case "organisation":{
                    //change organisation is a fragment and can't be loaded here so,
                    //go to dashboard activity and tell it to load change organisation fragment
                    i = new Intent(LandingActivity.this, DemoLists.class);
                    i.putExtra("redirectto" , "organisation");
                    startActivity(i);
                    finish();
                    break;
                }
                case "dashboard":{
                    //i = new Intent(SplashScreenActivity.this, DashboardActivity.class);
                    i = new Intent(LandingActivity.this, DemoLists.class);
                    i.putExtra("redirectto" , "dashboard");
                    startActivity(i);
                    finish();
                    break;
                }
                case "talktous":{
                    i = new Intent(LandingActivity.this, LandingActivity.class);
                    startActivity(i);
                    finish();
                    break;
                }
            }

        }else{
            switch(nextPage){
                case "profile":{
                    //go to profile activity
                    i = new Intent(LandingActivity.this, ProfileActivity.class);
                    startActivity(i);
                    finish();
                    break;
                }
                case "organisation":{
                    //change organisation is a fragment and can't be loaded here so,
                    //go to dashboard activity and tell it to load change organisation fragment
                    i = new Intent(LandingActivity.this, DashboardActivity.class);
                    i.putExtra("redirectto" , "organisation");
                    startActivity(i);
                    finish();
                    break;
                }
                case "dashboard":{
                    //i = new Intent(SplashScreenActivity.this, DashboardActivity.class);
                    i = new Intent(LandingActivity.this, DashboardActivity.class);
                    startActivity(i);
                    finish();
                    break;
                }
                case "talktous":{
                    i = new Intent(LandingActivity.this, LandingActivity.class);
                    startActivity(i);
                    finish();
                    break;
                }
            }
        }

    }


    public void openAuthenticationDialog()
    {
       /* AlertDialogPro.Builder builder = new AlertDialogPro.Builder(LandingActivity.this);
        mobileVerificationAlert= builder.create();

        builder.setCancelable(false);
        authenticationDialogView = getLayoutInflater().inflate(R.layout.activity_user_authentication, null);*/



        authenticationDialogView = new Dialog(LandingActivity.this);
        authenticationDialogView.requestWindowFeature(Window.FEATURE_NO_TITLE);

        authenticationDialogView.setContentView(R.layout.user_authentication);
        authenticationDialogView.setCancelable(false);

        eMobileNumber=(EditText)authenticationDialogView.findViewById(R.id.etMobileNumber);
        eOtp=(EditText)authenticationDialogView.findViewById(R.id.etOtp);
        verifyText=(TextView)authenticationDialogView.findViewById(R.id.tvVerifyText);

        verifyText.setVisibility(View.INVISIBLE);

        cancel=(Button)authenticationDialogView.findViewById(R.id.bCancel);
        proceed=(Button)authenticationDialogView.findViewById(R.id.bProceed);
        verify=(Button)authenticationDialogView.findViewById(R.id.bVerifyMobileNumber);
        imgVerify=(ImageView)authenticationDialogView.findViewById(R.id.imgVerify);
        imgVerify.setImageResource(R.drawable.verify_tick);
        imgVerify.setEnabled(false);
        proceed.setEnabled(false);
        verify.setEnabled(true);
        eMobileNumber.setEnabled(true);
        eMobileNumber.setText("");
        eOtp.setText("");
        verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(Master.isNetworkAvailable(LandingActivity.this))
                {
                    if (eMobileNumber.getText().toString().trim().length() != 10)
                        Toast.makeText(LandingActivity.this, R.string.toast_enter_valid_number, Toast.LENGTH_LONG).show();
                    else if (eMobileNumber.getText().toString().trim().charAt(0) == '0')
                        Toast.makeText(LandingActivity.this, R.string.toast_dont_prefix_zero_to_the_mobile_number, Toast.LENGTH_LONG).show();
                    else {
                        eMobileNumber.setEnabled(false);
                        verify.setEnabled(false);

                        JSONObject obj = new JSONObject();
                        try {

                            obj.put("phonenumber", "91" + eMobileNumber.getText().toString().trim());

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        new MobileNumberVerifyTask().execute(obj);
                    }
                }
                else
                {
                    Toast.makeText(LandingActivity.this, R.string.toast_Please_check_internet_connection, Toast.LENGTH_LONG).show();
                }
            }
        });


        imgVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                long session2 = System.currentTimeMillis();

                if (session2 < session) {
                    if(eOtp.getText().toString().trim().isEmpty())
                    {
                        imgVerify.setImageResource(R.drawable.verify_tick);
                        imgVerify.setColorFilter(Color.argb(255, 255, 255, 255));
                        imgVerify.setEnabled(true);
                        proceed.setEnabled(false);
                        verify.setEnabled(false);
                        verifyText.setVisibility(View.INVISIBLE);
                        Toast.makeText(LandingActivity.this, R.string.toast_enter_otp_received, Toast.LENGTH_LONG).show();

                    }
                    else if (eOtp.getText().toString().trim().equals(otp_check)) {
                        // changeMobileNumberAlert.dismiss();
                        countDownTimer.cancel();
                        imgVerify.setEnabled(false);
                        imgVerify.setImageResource(R.drawable.verify_tick);
                        imgVerify.setColorFilter(Color.argb(255, 90, 193, 67));
                        proceed.setEnabled(true);
                        verify.setEnabled(false);
                        verifyText.setVisibility(View.VISIBLE);
                        verifyText.setText(getString(R.string.textview_verify_tick_text));
                    } else {
                        imgVerify.setEnabled(true);
                        imgVerify.setImageResource(R.drawable.verify_wrong);
                        imgVerify.setColorFilter(Color.argb(255, 228, 8, 41));
                        proceed.setEnabled(false);
                        verify.setEnabled(false);
                        verifyText.setVisibility(View.VISIBLE);
                        verifyText.setText(getString(R.string.textview_verify_wrong_text));
                        Toast.makeText(LandingActivity.this, R.string.toast_Please_enter_correct_OTP, Toast.LENGTH_LONG).show();

                    }
                } else {
                    eOtp.setText("");
                    imgVerify.setImageResource(R.drawable.verify_tick);
                    imgVerify.setColorFilter(Color.argb(255, 255, 255, 255));
                    imgVerify.setEnabled(true);
                    proceed.setEnabled(false);
                    verify.setEnabled(true);
                    verifyText.setVisibility(View.INVISIBLE);
                    Toast.makeText(LandingActivity.this, R.string.toast_Sorry_your_OTP_expires___Try_to_get_new_OTP, Toast.LENGTH_LONG).show();
                }

            }
        });


        proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("phonenumber", "91" + eMobileNumber.getText().toString().trim());
                    jsonObject.put(Master.VERSION_CHECK_NEW_VERSION_TAG, Extra.version(getApplicationContext()));


//                    System.out.println("recover details for mobile number-------------" + jsonObject.toString());

                    new GetRegisteredUserDetailsTask().execute(jsonObject);

                } catch (JSONException e) {
                    Toast.makeText(LandingActivity.this, R.string.alert_something_went_wrong, Toast.LENGTH_LONG).show();
                }
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (countDownTimer != null)
                    countDownTimer.cancel();
                authenticationDialogView.dismiss();
                authenticationDialogView=null;

            }
        });

        //  builder.setView(authenticationDialogView);
        //  mobileVerificationAlert.show();

        authenticationDialogView.show();
    }




    public class MobileNumberVerifyTask extends AsyncTask<JSONObject, String, String> {



        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Material.circularProgressDialog(LandingActivity.this, getString(R.string.pd_sending_data_to_server), false);
        }

        @Override
        protected String doInBackground(JSONObject... params) {
            Master.getJSON = new GetJSON();
            Master.response = Master.getJSON.getJSONFromUrl(Master.getRegisteredUserMobileVerifyURL(), params[0], "POST",false,
                    null,null);
//            System.out.println(Master.response);
            return Master.response;
        }

        @Override
        protected void onPostExecute(String s) {

            if(Material.circularProgressDialog != null && Material.circularProgressDialog.isShowing())
                Material.circularProgressDialog.dismiss();

            if(s.equals("exception"))
            {
                Material.alertDialog(LandingActivity.this, getResources().getString(R.string.alert_cannot_connect_to_the_server), "OK");
                eMobileNumber.setEnabled(true);
                verify.setEnabled(true);
            }
            else
            {
                try
                {
                    Master.responseObject = new JSONObject(s);
                    String otp_val =  Master.responseObject.getString("otp");
                    String text =  Master.responseObject.getString("text");
                    imgVerify.setImageResource(R.drawable.verify_tick);
                    imgVerify.setColorFilter(Color.argb(255, 255, 255, 255));
                    imgVerify.setEnabled(true);
                    verifyText.setVisibility(View.INVISIBLE);

                    if(!otp_val.equals("null")&&!text.equals("null"))
                    {

                        System.out.println("otp value-----------"+otp_val);
                        Toast.makeText(LandingActivity.this,R.string.toast_OTP_has_sent_to_your_mobile_sms,Toast.LENGTH_LONG).show();

                        verify.setEnabled(false);
                        otp_check = otp_val;
                        session = System.currentTimeMillis();
                        countDownTimer = new OTPCountDownTimer(600000, 1000);
                        countDownTimer.start();

                        session = session + 600000; // 10 minutes

                    }
                    else
                    {
                        if(otp_val.equals("null")&&text.equals("User does not exist. Please get a referral"))
                        {
                            eMobileNumber.setEnabled(true);
                            verify.setEnabled(true);


                            Toast.makeText(LandingActivity.this,R.string.toast_user_does_not_exists, Toast.LENGTH_LONG).show();
                        }
                    }
                }
                catch (JSONException e)
                {
                    eMobileNumber.setEnabled(true);
                    verify.setEnabled(true);

                    Toast.makeText(LandingActivity.this, R.string.alert_something_went_wrong, Toast.LENGTH_LONG).show();
                }
            }



        }
    }





    public class GetRegisteredUserDetailsTask extends AsyncTask<JSONObject, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Material.circularProgressDialog(LandingActivity.this, getString(R.string.pd_sending_data_to_server), false);
        }

        @Override
        protected String doInBackground(JSONObject... params) {
            Master.getJSON = new GetJSON();
//            System.out.println("getdetails JSON: " + params[0]);
            Master.response = Master.getJSON.getJSONFromUrl(Master.getRegisteredUserDetailsURL(), params[0], "POST", false,
                    null,null);
//            System.out.println(Master.response);
            return Master.response;
        }

        @Override
        protected void onPostExecute(String response) {

            if(Material.circularProgressDialog.isShowing())
                Material.circularProgressDialog.dismiss();
            {
                if(response.equals("exception")){
                    Material.alertDialog(LandingActivity.this,getString(R.string.alert_cannot_connect_to_the_server), "OK");
                }
                else
                {
                    try
                    {

//                        System.out.println("recovery details are--------->"+response);


                        //no exception


                        try {
                            JSONObject responseJson = new JSONObject(response);

                            //store user details if present
                            storeUserDetails(responseJson);

                            //either  0,1 or 2
                            //0 -> no update required
                            //1 -> update recommended
                            //2 -> update necessary
                            String updateValue = responseJson.getString(Master.VERSION_CHECK_NEW_RESPONSE_UPDATE_REQUIRED_TAG);

                            switch (updateValue){
                                case "0":{
                                    // case -> no update needed
                                    //storing the update required in shared pref , if it is set to 0 , no need to update
                                    SharedPreferenceConnector.writeInteger(getApplicationContext(), Master.UpdateRequiredPref , 0);


                                    //response will contain details present flag only if we give the user mobile number if the api call
                                    if(responseJson.has(Master.VERSION_CHECK_NEW_RESPONSE_FLAG_TAG)) {


                                        //check if all details are present
                                        String flag = "";
                                        flag = responseJson.getString(Master.VERSION_CHECK_NEW_RESPONSE_FLAG_TAG);
                                        // all details present
                                        if (flag.equals("0")) {
                                            ShowStepper("dashboard");
                                        } else {
                                            //all details are not present , get details from user
                                            ShowStepper("profile");

                                        }
                                    }else{
                                        //mobile number of user was not present, so open talk to us page
                                        //openTalkToUs();
                                        //mobile number should be there

                                    }
                                    break;
                                }
                                case "1":{
                                    // case ->  update recommended

                                    //if all details are prenent or not will only be
                                    // returned if we give the mobile number to the api call
                                    if(responseJson.has(Master.VERSION_CHECK_NEW_RESPONSE_FLAG_TAG)) {

                                        //check if all details are present
                                        String flag = "";
                                        flag = responseJson.getString(Master.VERSION_CHECK_NEW_RESPONSE_FLAG_TAG);
                                        // all details present
                                        if (flag.equals("0")) {

                                            //
                                            goTo = "dashboard";

                                        } else {
                                            //all details are not present , get details from user
                                            goTo = "profile";
                                        }

                                        showRecommendUpdateDialog(2);
                                    }else{
                                        //mobile number of user was not present, so open talk to us page
                                        // openTalkToUs();

                                    }

                                    break;
                                }
                                case "2":{
                                    // case -> update compulsory
                                    forceUpdateDialog();
                                    break;

                                }

                            }


                        } catch (JSONException e) {
                            //show error message
                            Toast.makeText(getApplicationContext(), "An Error occurred, please try again",Toast.LENGTH_LONG).show();
                            //openTalkToUs();

                            e.printStackTrace();
                        }

                    }
                    catch (Exception e)
                    {
                        Toast.makeText(LandingActivity.this, R.string.alert_something_went_wrong, Toast.LENGTH_LONG).show();
                    }
                }
            }


        }
    }



    //----------------------- OTP Count Down Timer------------------------------------------------------

    public class OTPCountDownTimer extends CountDownTimer
    {

        public OTPCountDownTimer(long startTime, long interval)
        {
            super(startTime, interval);
        }

        @Override
        public void onFinish()
        {
            if(authenticationDialogView != null)
            {
                eOtp.setText("");
                imgVerify.setImageResource(R.drawable.verify_tick);
                imgVerify.setColorFilter(Color.argb(255, 255, 255, 255));
                imgVerify.setEnabled(false);
                verify.setEnabled(true);
                proceed.setEnabled(false);
                verifyText.setVisibility(View.INVISIBLE);

                Toast.makeText(LandingActivity.this, R.string.toast_Sorry_your_OTP_expires___Try_to_get_new_OTP, Toast.LENGTH_LONG).show();
            }
        }
        @Override
        public void onTick(long millisUntilFinished)
        {
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
//        Log.e("Landing page act", "in onResume()");
    }

    @Override
    protected void onPause() {
        super.onPause();
//        Log.e("Landing page act", "in onPause()");
        if(authenticationDialogView!= null&&authenticationDialogView.isShowing()){

        }else {
            finish();
        }
    }
}