package com.mobile.ict.cart.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.mobile.ict.cart.LokacartApplication;
import com.mobile.ict.cart.R;
import com.mobile.ict.cart.util.Master;


public class OrderSubmitActivity extends Activity implements View.OnClickListener{

    Button b1;
    TextView orderid;
    String orderId;
    private Tracker mTracker;
    private static final String TAG = "OrderSubmitActivity";
    String name = "OrderSubmit";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Obtain the shared Tracker instance.
        LokacartApplication application = (LokacartApplication) getApplication();
        mTracker = application.getDefaultTracker();
        Log.i(TAG, "Setting screen name: " + name);
        mTracker.setScreenName("Screen~" + name);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        setContentView(R.layout.activity_order_submit);

        orderId= getIntent().getExtras().getString("orderid");
        orderid=(TextView)findViewById(R.id.message2);
        orderid.setText(getResources().getString(R.string.label_activity_ordersubmit_orderid)+": "+orderId);
        b1 = (Button) findViewById(R.id.button_order_submit_done);
        b1.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if(view==b1){
            Intent i = new Intent(this,DashboardActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
            finishOrderSubmitActivity();
                    }
    }

    public void finishOrderSubmitActivity() {
        OrderSubmitActivity.this.finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Master.getMemberDetails(OrderSubmitActivity.this);
    }
}
